module.exports = {
    login: (req, res) => {
        res.render("pages/authentication/login", {
            layout: "layouts/authentication"
        })
    },

    logout: (req, res) => {
        req.logout(err => {
            if (err) return next(err)
            res.redirect("/auth/login")
        })
    },

    api: require("./api")
}