module.exports = {
    home: (req, res) => {
        let locals = {
            data: [
                {
                    Post: 10,
                    Visitor: 100,
                    Reader: 90,
                }
            ],
            contentName: "Statistic",
            layout: "layouts/dashboard"
        }

        res.render("pages/dashboard/home", locals)
    }
}