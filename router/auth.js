const router = require("express").Router()
const auth = require("../controllers/auth")

router.get("/login", auth.login)
router.get("/logout", auth.logout)
router.post("/login", auth.api.login)

module.exports = router