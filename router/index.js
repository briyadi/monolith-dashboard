const router = require("express").Router()
const controller = require("../controllers")
const auth = require("./auth")
const dashboard = require("./dashboard")

// Home
router.get("/", controller.home)

// Authentication pages
router.use("/auth", auth)

// Dashboard pages
router.use("/dashboard", dashboard)

// Error Handler
router.use(controller.notFound)
router.use(controller.exception)

module.exports = router