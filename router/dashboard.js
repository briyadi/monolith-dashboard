const router = require("express").Router()
const dashboard = require("../controllers/dashboard")
const authenticate = require("../middlewares/authenticate")

router.use(authenticate)
router.get("/", dashboard.home)

module.exports = router